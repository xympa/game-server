import { EntityRepository, Repository } from 'typeorm';
import { User } from './User.entity';
import * as bcrypt from 'bcrypt';
import { ConflictException, UnauthorizedException } from '@nestjs/common';

const hashPassword = (password: string, salt: string): Promise<string> =>
  bcrypt.hash(password, salt);

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(email: string, password: string): Promise<string> {
    try {
      const salt = await bcrypt.genSalt();

      const user: Omit<User, 'id'> = {
        salt: salt,
        password: await hashPassword(password, salt),
        email,
      };

      return ((await this.insert(user)).identifiers[0] as unknown) as string;
    } catch (err) {
      if (err.code === '23505')
        throw new ConflictException('Email is already registered');
      else throw err;
    }
  }

  async validateUser(email: string, password: string): Promise<User> {
    try {
      const user = await this.findOneOrFail({ email });

      if (!((await hashPassword(password, user.salt)) === user.password)) {
        throw new UnauthorizedException();
      } else {
        return user;
      }
    } catch (err) {
      throw err;
    }
  }
}
