import { Entity, Column, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
@Unique('UQ_Email', ['email'])
@Unique('UQ_DisplayName', ['displayName'])
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  email: string;

  @Column({ nullable: true })
  displayName?: string;

  @Column()
  password: string;

  @Column()
  salt: string;
}
