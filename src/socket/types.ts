import StrictEventEmitter from 'strict-event-emitter-types';
import { Socket } from 'socket.io';

export interface LobbyPlayer {
  id: string;
  displayName: string;
}

interface ServerSentEvents {
  connect: void;
  lobbyRefresh: LobbyPlayer[];
  badAuth: void;
  requestDisplayNameSet: void;
  displayNameChanged: string;
  displayNameAlreadyInUse: void;
  youHaveBeenChallenged: string;
  challengeFailed: void;
}

export type ClientSentEvents = {
  login: string;
  setDisplayName: string;
  issueChallenge: string;
};

export type ClientEmitter = StrictEventEmitter<
  Socket,
  ServerSentEvents,
  ClientSentEvents
>;

export type ServerEmitter = StrictEventEmitter<
  Socket,
  ClientSentEvents,
  ServerSentEvents
>;
