import { Module } from '@nestjs/common';
import { ServerGateway } from './server.gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from '../authentication/User.repository';
import { AuthenticationModule } from '../authentication/authentication.module';

@Module({
  imports: [TypeOrmModule.forFeature([UserRepository]), AuthenticationModule],
  controllers: [],
  providers: [ServerGateway],
})
export class SocketModule {}
