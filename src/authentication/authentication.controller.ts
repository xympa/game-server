import { Controller, Post, Body } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { RegisterUserDto } from './dto/register-user.dto';
import { UserRegisterReturn } from './authentication.service';

@Controller('authentication')
export class AuthenticationController {
  constructor(private authenticationService: AuthenticationService) {}

  @Post('/sign-up')
  signup(@Body() registerDto: RegisterUserDto): Promise<UserRegisterReturn> {
    return this.authenticationService.register(registerDto);
  }

  @Post('/sign-in')
  signin(@Body() registerDto: RegisterUserDto): Promise<UserRegisterReturn> {
    return this.authenticationService.signin(registerDto);
  }
}
