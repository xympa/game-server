import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketServer,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { AuthenticationService } from '../authentication/authentication.service';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../authentication/User.entity';
import { UserRepository } from '../authentication/User.repository';
import {
  ServerEmitter,
  ClientEmitter,
  ClientSentEvents,
  LobbyPlayer,
} from './types';
import { Server } from 'socket.io';

interface UnknownPlayerConnection {
  loggedIn: false;
}
interface LoggedInPlayerConnection {
  id: string;
  loggedIn: true;
  requestingLobby: boolean;
  displayName: string | null;
  challenging: string | null;
  challengedBy: string | null;
}

type PlayerConnection = UnknownPlayerConnection | LoggedInPlayerConnection;

@WebSocketGateway<ClientEmitter>()
export class ServerGateway implements OnGatewayConnection, OnGatewayDisconnect {
  constructor(
    private readonly authenticationService: AuthenticationService,
    @InjectRepository(User)
    private usersRepository: UserRepository,
  ) {}

  private readonly clients = new Map<ServerEmitter, PlayerConnection>();
  private readonly logger = new Logger('ServerGateway', true);

  @WebSocketServer()
  server: Server;

  handleConnection(client: ServerEmitter): void {
    this.clients.set(client, { loggedIn: false });
    this.logger.log(`${client.id} connected`);
    this.logger.verbose(`Total connections are now: ${this.clients.size}`);
  }

  handleDisconnect(client: ServerEmitter): void {
    const info = this.clients.get(client);

    if (info.loggedIn && info.challenging) {
      const socket = this.findSocketById(info.challenging);
      socket.emit('challengeFailed');
    } else if (info.loggedIn && info.challengedBy) {
      const socket = this.findSocketById(info.challengedBy);
      socket.emit('challengeFailed');
    }

    this.clients.delete(client);
    this.logger.log(`${client.id} disconnected`);
    this.logger.verbose(`Total connections are now: ${this.clients.size}`);

    this.refreshLobbyPlayers();
  }

  @SubscribeMessage<keyof ClientSentEvents>('setDisplayName')
  async setDisplayName(
    client: ServerEmitter,
    data: ClientSentEvents['setDisplayName'],
  ): Promise<void> {
    const player = this.clients.get(client);

    if (!player || !player.loggedIn) {
      client.emit('badAuth');
      return;
    }

    const user = await this.usersRepository.findOne(player.id);

    if (!user) {
      client.emit('badAuth');
      return;
    }

    try {
      await this.usersRepository.update(user.id, { displayName: data });
    } catch (err) {
      client.emit('displayNameAlreadyInUse');
      return;
    }

    this.clients.set(client, {
      ...player,
      requestingLobby: true,
      displayName: data,
    });

    client.emit('displayNameChanged', data);

    this.refreshLobbyPlayers();
  }

  @SubscribeMessage<keyof ClientSentEvents>('login')
  async login(
    client: ServerEmitter,
    jwt: ClientSentEvents['login'],
  ): Promise<void> {
    const payload = this.authenticationService.verifyJwt(jwt);

    if (!payload) {
      //jwt invalid
      client.emit('badAuth');
    } else {
      const user = await this.usersRepository.findOne(payload.userId);

      this.clients.set(client, {
        id: user.id,
        loggedIn: true,
        requestingLobby: !!user.displayName,
        displayName: user.displayName,
        challengedBy: null,
        challenging: null,
      });
      this.logger.log(`${user.id} logged in`);

      if (!user.displayName) {
        client.emit('requestDisplayNameSet');
      } else {
        this.refreshLobbyPlayers();
        client.emit('displayNameChanged', user.displayName);
      }
    }
  }

  async refreshLobbyPlayers(): Promise<void> {
    this.logger.verbose(`Refreshing lobby players`);

    const players: LobbyPlayer[] = [];

    const toWarn: [ServerEmitter, PlayerConnection][] = [];

    for (const [socket, player] of this.clients.entries()) {
      if (player.loggedIn && player.requestingLobby) {
        players.push({ displayName: player.id, id: player.id });
        toWarn.push([socket, player]);
      }
    }

    const users = await this.usersRepository.find({
      where: players.map(pl => ({ id: pl.id })),
      select: ['id', 'displayName'],
    });

    for (const player of players) {
      player.displayName =
        users.find(user => player.id === user.id).displayName || player.id;
    }

    for (const [client, player] of toWarn) {
      client.emit(
        'lobbyRefresh',
        players.filter(
          filteredPlayer => player.loggedIn && filteredPlayer.id !== player.id,
        ),
      );
    }

    this.logger.verbose(`Lobby refresh emitted`);
  }

  @SubscribeMessage<keyof ClientSentEvents>('issueChallenge')
  issueChallenge(
    client: ServerEmitter,
    targetId: ClientSentEvents['issueChallenge'],
  ): void {
    const challenger = this.clients.get(client);

    if (!challenger.loggedIn) {
      client.emit('badAuth');
      return;
    }

    let entry: [ServerEmitter, LoggedInPlayerConnection] | undefined;

    for (const [socket, player] of this.clients.entries()) {
      if (player.loggedIn && player.requestingLobby && player.id === targetId) {
        entry = [socket, player];
        break;
      }
    }

    if (!entry) {
      client.emit('challengeFailed');
      return;
    }

    entry[0].emit(
      'youHaveBeenChallenged',
      challenger.loggedIn && challenger.displayName,
    );

    this.clients.set(client, { ...challenger, challenging: entry[1].id });

    this.clients.set(entry[0], {
      ...entry[1],
      challengedBy: (challenger.loggedIn && challenger.id) || null,
    });
  }

  private findSocketById(id: string): undefined | ServerEmitter {
    for (const [socket, player] of this.clients.entries()) {
      if (player.loggedIn && player.id === id) return socket;
    }

    return undefined;
  }
}
