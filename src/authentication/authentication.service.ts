import { Injectable } from '@nestjs/common';
import { User } from './User.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './User.repository';
import { RegisterUserDto } from './dto/register-user.dto';
import { JwtPayload } from './jwt-payload';
import { JwtService } from '@nestjs/jwt';

export interface UserRegisterReturn {
  token: string;
}

@Injectable()
export class AuthenticationService {
  constructor(
    @InjectRepository(User)
    private usersRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async register(registerDto: RegisterUserDto): Promise<UserRegisterReturn> {
    const { email, password } = registerDto;

    const createdId = await this.usersRepository.createUser(email, password);

    const payload: JwtPayload = { userId: createdId };

    return {
      token: this.jwtService.sign(payload),
    };
  }

  async signin(registerDto: RegisterUserDto): Promise<UserRegisterReturn> {
    const { email, password } = registerDto;

    const user = await this.usersRepository.validateUser(email, password);

    const payload: JwtPayload = { userId: user.id };

    return {
      token: this.jwtService.sign(payload),
    };
  }

  verifyJwt(jwt: string): JwtPayload | undefined {
    try {
      return this.jwtService.verify<JwtPayload>(jwt);
    } catch (err) {
      return undefined;
    }
  }
}
